import gulp from 'gulp'
import { browserSync } from './browserSync.js'

export const html = () => {
    return gulp.src('./src/*.html').pipe(gulp.dest('./dist')).pipe(browserSync.stream())
}